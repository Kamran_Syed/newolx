<?php 
/* 
Template Name: Page For Products
*/ 
?>

<?php get_header(); ?>
<div id="wrapper">
	<div class="tw-bs container m_down">
		<div class="mega-menu mega-menu-horizontal">
				<?php contextual_nav_menu_breadcrumb(); ?>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						
					<article class="post" id="post-<?php the_ID(); ?>">
						<section class = "aspk_section" >
						<div class="entry">

							<?php the_content(); ?>

							<?php //wp_link_pages(array('before' => __('Pages: ','html5reset'), 'next_or_number' => 'number')); ?>

						</div>

						<?php //edit_post_link(__('Edit this entry','html5reset'), '<p>', '</p>'); ?>
						</section>
					</article>
					
					<?php //comments_template(); ?>
			<?php endwhile; endif; ?>
			</div>
		</div>
	</div><!-- end container -->
</div><!-- end wrapper -->

<?php get_footer(); ?>
