<script>
	jQuery( document ).ready(function() {
		jQuery('form#searchform > div > #s').focusin(function(){
			jQuery(this).attr('placeholder','');
		});
		jQuery('form#searchform > div > #s').blur(function(){
			jQuery(this).attr('placeholder','Search for products');
		});
	});
</script>
<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
    <div>
       <!-- <label for="s" class="screen-reader-text"><?php _e('Search for:','html5reset'); ?></label> -->
        <input class="search_field"  type="search" id="s" name="s" value="" placeholder="Search for products" />
        
      <!--  <input type="submit" value="<?php _e('Search','html5reset'); ?>" id="searchsubmit" /> -->
    </div>
</form>