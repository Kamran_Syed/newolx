<?php
/*
Template name: Right sidebar
*/
get_header(); ?>
<div id="wrapper">
	<div class="tw-bs container">
		<div class="row">
			<div class="col-md-8">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						
					<article class="post" id="post-<?php the_ID(); ?>">

						<h2><?php the_title(); ?></h2>
						<div class="entry">

							<?php the_content(); ?>

							<?php wp_link_pages(array('before' => __('Pages: ','html5reset'), 'next_or_number' => 'number')); ?>

						</div>

						<?php edit_post_link(__('Edit this entry','html5reset'), '<p>', '</p>'); ?>

					</article>
					
					<?php comments_template(); ?>
				<?php endwhile; endif; ?>
			</div>
			<div class="col-md-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div><!-- end container -->
			
</div><!-- end wrapper -->