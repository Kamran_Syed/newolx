<?php 
/* 
Template Name: Page Content 
*/ 
 get_header(); ?>
<div id="wrapper">
	<div class="tw-bs container minheight">
	<div class="mega-menu mega-menu-horizontal">
				<?php contextual_nav_menu_breadcrumb(); ?>
	</div>
	<div class="row">
			<div class="col-md-1" ></div>
			<div class="col-md-8" >
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						
					<article class="post" id="post-<?php the_ID(); ?>">
						<section class = "aspk_section" >
						<h2 class="page_heading"><?php the_title(); ?></h2>
						<div class="entry">

							<?php the_content(); ?>

							<?php //wp_link_pages(array('before' => __('Pages: ','html5reset'), 'next_or_number' => 'number')); ?>

						</div>

						<?php //edit_post_link(__('Edit this entry','html5reset'), '<p>', '</p>'); ?>
						</section>
					</article>
					
					<?php //comments_template(); ?>
			<?php endwhile; endif; ?>
			</div>
			<div class="col-md-3" ></div>
		</div>
	</div><!-- end container -->
</div><!-- end wrapper -->

<?php get_footer(); ?>