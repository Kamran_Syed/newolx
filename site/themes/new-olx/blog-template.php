<?php
/* 
Template Name: Blog Page
*/ 
?>
<?php get_header(); ?>
<div id="wrapper">
	<div class="tw-bs container minheight">
		<div class="mega-menu mega-menu-horizontal">
				<?php contextual_nav_menu_breadcrumb(); ?>
		</div>
		<style>
			ul {
				list-style: inherit !important;
			}
		</style>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-7">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						
					<article class="post" id="post-<?php the_ID(); ?>">

						<h2 class="page_heading"><?php the_title(); ?></h2>
						<div class="entry">

							<?php the_content(); ?>

							<?php //wp_link_pages(array('before' => __('Pages: ','html5reset'), 'next_or_number' => 'number')); ?>

						</div>

						<?php //edit_post_link(__('Edit this entry','html5reset'), '<p>', '</p>'); ?>

					</article>
					
					<?php //comments_template(); ?>
			<?php endwhile; endif; ?>
			</div>
			<div class="col-md-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div><!-- end container -->
</div><!-- end wrapper -->

<?php get_footer(); ?>
