<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
?>		
		<div class="footer-wrap">
			<footer id="footer"  role="contentinfo">
				<div class="tw-bs container">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-7">
									<?php 
									 if ( has_nav_menu( 'menu-footer' ) ) { 
											wp_nav_menu( array( 'theme_location' => 'menu-footer', 'menu_class' => 'aspk_nav_menu_footer'  ) ); 
										} ;
									?>
								</div>
								<div class = "col-md-5">
									<form method = "POST" action = "#">
										<span class="footer_span" >
											<span class="f_left">Stay in touch</span>
											<span class="f_left"><input onfocus="this.placeholder = ''" onblur="this.placeholder = 'e-mail address'" id = "mail_input" class="footer_mail_field " type = "text" name = "email" placeholder = "e-mail address" required/>
											</span>
										</span>
										<span class="footer_span"><input class="footer_field" type = "submit" name = "submit_email" value = "Go" /></span>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</footer>
			<?php 
			?>
		</div>
		<script>
		jQuery( document ).ready(function() {
			jQuery('#romove_cart_title').hide();
			jQuery('.screen-reader-text').hide();
			jQuery('#searchsubmit').hide();
			jQuery('#P_MS549a580232197').css('min-width','1090px');
			jQuery('#P_MS549a580232197').css('margin-left', '0px');
			jQuery('.woocommerce-message > .wc-forward').attr('id', 'aspk_newcart');
			jQuery('#mail_input').focusin(function(){
				jQuery(this).css('outline','none');
			});
			var woocommerce_msg = jQuery('.woocommerce-info').html();
			if(woocommerce_msg == 'No products were found matching your selection.'){
				jQuery('#container').addClass('tw-bs container');
			}
			
		});
		</script>

	<?php wp_footer(); ?>


<!-- jQuery is called via the WordPress-friendly way via functions.php -->

<!-- this is where we put our custom functions -->
<script src="<?php bloginfo('template_directory'); ?>/_/js/functions.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/tiptip.js"></script>



</body>

</html>
