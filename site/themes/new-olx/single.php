<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>
<div id="wrapper">
	<div class="tw-bs container minheight">
		<div class="mega-menu mega-menu-horizontal">
				<?php contextual_nav_menu_breadcrumb(); ?>
		</div>
		<style>
			ul {
				list-style: inherit !important;
			}
		</style>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-7 bottom-border">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
					
					<h2 class="page_heading"><?php the_title(); ?></h2>

					<div class="entry-content">
						
						<?php the_content(); ?>

						<?php wp_link_pages(array('before' => __('Pages: ','html5reset'), 'next_or_number' => 'number')); ?>
						
						<?php the_tags( __('Tags: ','html5reset'), ', ', ''); ?>
					
						

					</div>
						<?php edit_post_link(__('Edit this entry','html5reset'),'','.'); ?>
					
				</article>
			</div>
			<div class="col-md-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
		<?php posted_on(); ?>
		<?php comments_template(); ?>
	</div><!-- end container -->
</div><!-- end wrapper -->
<script>
	jQuery( document ).ready(function() {
		jQuery('.current-menu-ancestor').hide();
	});
</script>

	<?php endwhile; endif; ?>

<?php post_navigation(); ?>

<?php get_footer(); ?>