<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
?><!DOCTYPE html>

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

<head id="<?php echo of_get_option('meta_headid'); ?>" data-template-set="html5-reset-wordpress-theme">

	<meta charset="<?php bloginfo('charset'); ?>">

	<!-- Always force latest IE rendering engine (even in intranet) -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->

	<?php
		if (is_search()) echo '<meta name="robots" content="noindex, nofollow" />';
	?>

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<meta name="title" content="<?php wp_title( '|', true, 'right' ); ?>">

	<!--Google will often use this as its description of your page/site. Make it good.-->
	<meta name="description" content="<?php bloginfo('description'); ?>" />

	<?php
		if (true == of_get_option('meta_author'))
			echo '<meta name="author" content="' . of_get_option("meta_author") . '" />';

		if (true == of_get_option('meta_google'))
			echo '<meta name="google-site-verification" content="' . of_get_option("meta_google") . '" />';
	?>

	<meta name="Copyright" content="Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">

	<?php
		/*
			j.mp/mobileviewport & davidbcalhoun.com/2010/viewport-metatag
			 - device-width : Occupy full width of the screen in its current orientation
			 - initial-scale = 1.0 retains dimensions instead of zooming out if page height > device height
			 - maximum-scale = 1.0 retains dimensions instead of zooming in if page width < device width
			 - minimal-ui = iOS devices have minimal browser ui by default
		*/
		if (true == of_get_option('meta_viewport'))
			echo '<meta name="viewport" content="' . of_get_option("meta_viewport") . ' minimal-ui" />';


		/*
			These are for traditional favicons and Android home screens.
			 - sizes: 1024x1024
			 - transparency is OK
			 - see wikipedia for info on browser support: http://mky.be/favicon/
			 - See Google Developer docs for Android options. https://developers.google.com/chrome/mobile/docs/installtohomescreen
		*/
		if (true == of_get_option('head_favicon')) {
			echo '<meta name=”mobile-web-app-capable” content=”yes”>';
			echo '<link rel="shortcut icon" sizes=”1024x1024” href="' . of_get_option("head_favicon") . '" />';
		}


		/*
			The is the icon for iOS Web Clip.
			 - size: 57x57 for older iPhones, 72x72 for iPads, 114x114 for iPhone4 retina display (IMHO, just go ahead and use the biggest one)
			 - To prevent iOS from applying its styles to the icon name it thusly: apple-touch-icon-precomposed.png
			 - Transparency is not recommended (iOS will put a black BG behind the icon) -->';
		*/
		if (true == of_get_option('head_apple_touch_icon'))
			echo '<link rel="apple-touch-icon" href="' . of_get_option("head_apple_touch_icon") . '">';
	?>

	<!-- concatenate and minify for production -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/reset.css" />
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,400,300,600|Josefin+Sans' rel='stylesheet' type='text/css' data-noprefix>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css">

	<!-- Lea Verou's Prefix Free, lets you use only un-prefixed properties in yuor CSS files -->
    <script src="<?php echo get_template_directory_uri(); ?>/_/js/prefixfree.min.js"></script>

	<!-- This is an un-minified, complete version of Modernizr.
		 Before you move to production, you should generate a custom build that only has the detects you need. -->
	<script src="<?php echo get_template_directory_uri(); ?>/_/js/modernizr-2.8.0.dev.js"></script>

	<!-- Application-specific meta tags -->
	<?php
		// Windows 8
		if (true == of_get_option('meta_app_win_name')) {
			echo '<meta name="application-name" content="' . of_get_option("meta_app_win_name") . '" /> ';
			echo '<meta name="msapplication-TileColor" content="' . of_get_option("meta_app_win_color") . '" /> ';
			echo '<meta name="msapplication-TileImage" content="' . of_get_option("meta_app_win_image") . '" />';
		}

		// Twitter
		if (true == of_get_option('meta_app_twt_card')) {
			echo '<meta name="twitter:card" content="' . of_get_option("meta_app_twt_card") . '" />';
			echo '<meta name="twitter:site" content="' . of_get_option("meta_app_twt_site") . '" />';
			echo '<meta name="twitter:title" content="' . of_get_option("meta_app_twt_title") . '">';
			echo '<meta name="twitter:description" content="' . of_get_option("meta_app_twt_description") . '" />';
			echo '<meta name="twitter:url" content="' . of_get_option("meta_app_twt_url") . '" />';
		}

		// Facebook
		if (true == of_get_option('meta_app_fb_title')) {
			echo '<meta property="og:title" content="' . of_get_option("meta_app_fb_title") . '" />';
			echo '<meta property="og:description" content="' . of_get_option("meta_app_fb_description") . '" />';
			echo '<meta property="og:url" content="' . of_get_option("meta_app_fb_url") . '" />';
			echo '<meta property="og:image" content="' . of_get_option("meta_app_fb_image") . '" />';
		}
	?>

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php wp_head(); ?>
	<style>
		#mega-menu-wrap-top_bar_nav-24 #mega-menu-top_bar_nav-24 > li.mega-menu-item > a {
			font: 12px 'FuturaStdMedium';
		}
		#mega-menu-wrap-top_bar_nav-24 #mega-menu-top_bar_nav-24 {
			padding: 10px 0px 0px 0px;
		}
		#mega-menu-wrap-top_bar_nav-24 #mega-menu-top_bar_nav-24 > li.mega-menu-flyout ul.mega-sub-menu li.mega-menu-item a {
			font: 12px 'FuturaStdMedium';
		}
		#mega-menu-wrap-aspk_header-22 #mega-menu-aspk_header-22 > li.mega-menu-item > a {
			padding: 0px 0px 0px 7px;
		}
		.breadcrumb > li + li:before {
			content: ">";
			color:#a7a9ac;
		}
	</style>
	<script>
	jQuery( document ).ready(function() {
		jQuery('#content > #mega-menu-aspk_header-22').css('display','inline-flex'); 
		jQuery('#content > #mega-menu-aspk_header-22').css('color','#bcbec0'); 
		jQuery('#content > #mega-menu-aspk_header-22 > li').css('margin-bottom','1em'); 
		jQuery('#content > #mega-menu-aspk_header-22').css('border-bottom','1px solid #bcbec0'); 
		jQuery('#content > #mega-menu-aspk_header-22').css('width','100%'); 
		jQuery('#content > #mega-menu-aspk_header-22').css('text-transform','uppercase');  
		jQuery('#sidebar').hide();
	});
	</script>

</head>

<body <?php body_class(); ?>>
	<?php aspk_check_login();?>
	
	<header id="header">
		<div class="tw-bs container"><!-- start header-container -->
			<div class="header-top ">
					<div class="float_right">
						<div  class="left_class">
							<?php  if ( has_nav_menu( 'account_log' ) ) { 
										wp_nav_menu( array('theme_location' => 'account_log', 'menu_class' => 'aspk_nav_menu')); 
									} ; ?>
						</div>
						<div class="left_class">
							<div class="row">
								<div class="col-md-6" id="mini_top_color" >
									<?php if ( is_active_sidebar( 'product_sidebar' ) ) { 
											dynamic_sidebar('product_sidebar');
										} ?> 
								</div>
								<?php  ?>
								<div class="col-md-6">
									<a  id="aspk_logo" href="<?php echo home_url().'/cart/'; ?>">
										<img  id="aspk_tokri" class="cart_image" src="<?php echo get_bloginfo('template_url') ?>/images/s-cart.png"/>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div style="clear:right;float:right;">
							<?php if ( is_active_sidebar( 'header-search' ) ) { 
										dynamic_sidebar('header-search');
								 } ?> 
					</div>	
					<div class="clear_both" style="margin-left:22em;">
							<a  id="aspk_logo" href="<?php echo home_url(); ?>">
								<img src="<?php echo get_bloginfo('template_url') ?>/images/logo.png" alt="New-Olx.com"/>
							</a>
					</div>
					<div class="row">
						<div class="col-md-8">
							<?php  if ( has_nav_menu( 'aspk_header' ) ) { 
										wp_nav_menu( array('theme_location' => 'aspk_header')); 
									} ; ?>
								<?php //wp_nav_menu( array('menu' => 'primary' , 'menu_class' => 'main-top-bar-nav' )); ?>
						</div>
						<div class="col-md-4">
						<?php	if ( has_nav_menu( 'top_bar_nav' ) ) {
									wp_nav_menu( array('theme_location' => 'top_bar_nav' )); 
								} ?>
						</div>
					</div>
			</div>
		</div><!-- end header-container -->
	</header>