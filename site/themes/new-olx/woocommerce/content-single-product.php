<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $product,$aspk_social_shared_links;
$post_title = $product->post->post_title;
$post_desc = $product->post->post_content;
$post_url = $product->post->guid;
$img_id = get_post_thumbnail_id($product->id );
$img_src = wp_get_attachment_image_src($img_id); 
$post_image = $img_src[0];

	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		/**
		 * woocommerce_before_single_product_summary hook
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary">

		<?php
			/**
			 * woocommerce_single_product_summary hook
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			do_action( 'woocommerce_single_product_summary' );
			
			?>
				<div class="agile_social_link"><?php echo $aspk_social_shared_links->aspk_social_links($post_url,$post_title,$post_desc,$post_image); ?></div>
			<?php
			require_once('single-product/tabs/description-product.php');
			
				?>
					<span class="dimension_heading">CONDITION</span>
				<?php 
					$condition = get_post_meta( $product->id, '_aspk_condition' , true);
				?>
					<div><?php echo $condition ; ?></div>
					<span class="dimension_heading">Dimensions</span>
				<?php 
				$width = get_post_meta( $product->id, '_width', true );
				$height = get_post_meta( $product->id, '_height', true );
				$length = get_post_meta( $product->id, '_length', true );
				$other_dimen = get_post_meta( $product->id, '_other_dimension' , true);
				$sku_pro = $product->get_sku();
				if(! empty($width) || ! empty($height)) { ?>
					<div><?php echo $width .'″ W  x  '.$height.'″ H '; ?></div>
				<?php }
				if(! empty($other_dimen) || ! empty($length)) { ?>
					<div><?php echo $other_dimen .'″ deep,'.$length.'″ seat depth'; ?></div>
				<?php } ?>
				<div class="agile_product_meta">
					<span class="sku_span">
						<span class="f-bold">SKU:</span>
						<span class="aspk_sku" ><?php echo $sku_pro; ?></span>
						<span class="f-bold">Categories:</span>
						<span id="aspk_remove_comma">
							<?php $terms = get_the_terms($product->id ,  'product_cat' ); 
							foreach($terms as $term){
								echo $term->slug .',';
							}
							?>
						</span>
						<script>
						jQuery( document ).ready(function() {
							var x = jQuery('#aspk_remove_comma').text();
							var str = x.replace(/,\s*$/, ".");
							jQuery('#aspk_remove_comma').text(str);
						});
						</script>
					</span>
					<span class="sku_span" >
						<span class="f-bold">Tags:</span>
						<span class="aspk_tags" >
						<?php echo $product->get_tags(); ?>
						</span>
						<span> <?php
						 ?>
						</span>
					</span>
				</div>
				<?php
		?>

	</div><!-- .summary -->

	<?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_output_related_products - 20
		 */
		//do_action( 'woocommerce_after_single_product_summary' );
		
	?>
	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->
<div class="agile_related_product">
	<?php require_once('single-product/related.php'); ?>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
