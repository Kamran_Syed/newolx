<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $product;
?>
<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">

	<p class="price">
		<span  class="reg_price_text">ORIGINAL PRICE :</span>
		<span class="reg_price"><?php echo wc_price($product->get_regular_price()); ?></span>
	</p>
	<div class="aspk_sale_price"  style="clear:both; ">
		<div class="our_p">OUR PRICE :</div>
		<div class="our_p_text"><?php echo wc_price($product->get_sale_price()); ?></div>
	</div>
	<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
	<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />

</div>