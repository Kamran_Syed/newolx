<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() ); ?>

<form name="checkout" method="post" class="checkout" action="<?php echo esc_url( $get_checkout_url ); ?>" enctype="multipart/form-data">

	<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">

			<div class="col-1">

				<?php do_action( 'woocommerce_checkout_billing' ); ?>

			</div>

			<div class="col-2">
				<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

					<h3 id="order_review_heading"><?php _e( 'Your order', 'woocommerce' ); ?></h3>

				<?php endif; ?>

				<?php do_action( 'woocommerce_checkout_order_review' ); ?>
				

			</div>

		</div>
		<div class="col2-set" id="customer_details">
			<div class="col-1">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
		</div>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
<script>
heading = jQuery(".page_heading").html();
uper_heading = heading.toUpperCase();
jQuery(".page_heading").html(uper_heading);
jQuery('.aspk_section').css('width','650px');
jQuery('.aspk_section').css('margin-left','150px');
jQuery('.woocommerce-info').css('border-top-color','#666666');
// billing country field edit
jQuery('#billing_country_field').html('<label class="">Country</label><strong>United States (US)</strong><input type="hidden" class="country_to_state" value="US" id="billing_country" name="billing_country">');
// firest name field edit
jQuery('#billing_first_name_field').removeClass('form-row-first');
jQuery('#billing_first_name_field').css('text-transform','lowercase');
jQuery('#billing_first_name_field').addClass('form-row-wide');
jQuery('#billing_first_name').addClass('form-control');
// last name field edit
jQuery('#billing_last_name_field').removeClass('form-row-last');
jQuery('#billing_last_name_field').css('text-transform','lowercase');
jQuery('#billing_last_name_field').addClass('form-row-wide');
jQuery('#billing_last_name').addClass('form-control');
// company name field edit
jQuery('#billing_company_field').css('text-transform','lowercase');
jQuery('#billing_company').addClass('form-control');
// billing name field edit
jQuery('#billing_address_1_field').css('text-transform','lowercase');
jQuery('#billing_address_1').addClass('form-control');
jQuery('#billing_address_2').addClass('form-control');
// billing name field edit
jQuery('#billing_city_field').css('text-transform','lowercase');
jQuery('#billing_city').addClass('form-control');
// billing state name field edit
jQuery('#billing_state_field').removeClass('form-row-first address-field');
jQuery('#billing_first_name_field').addClass('form-row-wide');
jQuery('#billing_state_field').css('text-transform','lowercase');
jQuery('#billing_state_field label').html('state');
jQuery('#billing_state').addClass('form-control');
// billing postcode name field edit
jQuery('#billing_postcode_field').removeClass('form-row-last address-field');
jQuery('#billing_postcode_field').addClass('form-row-wide');
jQuery('#billing_postcode_field').css('text-transform','lowercase');
jQuery('#billing_postcode').addClass('form-control');
// billing email field edit
jQuery('#billing_email_field').removeClass('form-row form-row-first');
jQuery('#billing_email_field').addClass('form-row-wide');
jQuery('#billing_email_field').css('text-transform','lowercase');
jQuery('#billing_email').addClass('form-control');
// billing phone field edit
jQuery('#billing_phone_field').removeClass('form-row-last');
jQuery('#billing_phone_field').addClass('form-row-wide');
jQuery('#billing_phone_field').css('text-transform','lowercase');
jQuery('#billing_phone').addClass('form-control');
jQuery('#billing_phone_field label').append('(where we can cantact you to arrange for shipping)');
// shipping country field edit
jQuery('#shipping_country_field').html('<label class="">Country</label><strong>United States (US)</strong><input type="hidden" class="country_to_state" value="US" id="shipping_country" name="shipping_country">');
// shipping first name field edit
jQuery('#shipping_first_name_field').removeClass('form-row-first');
jQuery('#shipping_first_name_field').css('text-transform','lowercase');
jQuery('#shipping_first_name_field').addClass('form-row-wide');
jQuery('#shipping_first_name').addClass('form-control');
//shipping last name field edit
jQuery('#shipping_last_name_field').removeClass('form-row-last');
jQuery('#shipping_last_name_field').css('text-transform','lowercase');
jQuery('#shipping_last_name_field').addClass('form-row-wide');
jQuery('#shipping_last_name').addClass('form-control');
//shipping company name field edit
jQuery('#shipping_company_field').css('text-transform','lowercase');
jQuery('#shipping_company').addClass('form-control');
// shipping address field edit
jQuery('#shipping_address_1_field').css('text-transform','lowercase');
jQuery('#shipping_address_1').addClass('form-control');
jQuery('#shipping_address_2').addClass('form-control');
// shipping field edit
jQuery('#shipping_city_field').css('text-transform','lowercase');
jQuery('#shipping_city').addClass('form-control');
// shipping state name field edit
jQuery('#shipping_state_field').removeClass('form-row-first address-field');
jQuery('#shipping_state_field').addClass('form-row-wide');
jQuery('#shipping_state_field').css('text-transform','lowercase');
jQuery('#shipping_state').addClass('form-control');
// shipping postcode name field edit
jQuery('#shipping_postcode_field').removeClass('form-row-last address-field');
jQuery('#shipping_postcode_field').addClass('form-row-wide');
jQuery('#shipping_postcode_field').css('text-transform','lowercase');
jQuery('#shipping_postcode').addClass('form-control');
//gateway remove class expiration date field
jQuery(".form-row").css('font-family','futurastdbook');
jQuery(".page_heading").css('font-family','futurastdbook');

</script>