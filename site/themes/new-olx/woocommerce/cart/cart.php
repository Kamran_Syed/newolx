<?php
/**
 * Cart Page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce;

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>

<form action="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" method="post">

<?php do_action( 'woocommerce_before_cart_table' ); ?>

<table class="shop_table cart" style="border: medium hidden;" cellspacing="0" >
	<thead>
		<tr>
			<th class="product-remove" style="border-bottom: 2px solid #e7e7e7;">&nbsp;</th>
			<th class="product-thumbnail" style="border-bottom: 2px solid #e7e7e7;">&nbsp;</th>
			<th class="product-name" style = "border-bottom: 2px solid #e7e7e7; color: #bcbec0;font-family: futurastdbook;"><?php _e( 'PRODUCT', 'woocommerce' ); ?></th>
			<th class="product-price" style = "border-bottom: 2px solid #e7e7e7; color: #bcbec0;font-family: futurastdbook;"><?php _e( 'PRICE', 'woocommerce' ); ?></th>
			<th class="product-quantity" style = "border-bottom: 2px solid #e7e7e7; color: #bcbec0;font-family: futurastdbook;"><?php _e( 'QUANTITY', 'woocommerce' ); ?></th>
			<th class="product-subtotal" style = "border-bottom: 2px solid #e7e7e7; color: #bcbec0;font-family: futurastdbook;"><?php _e( 'TOTAL', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php do_action( 'woocommerce_before_cart_contents' ); ?>

		<?php
		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				?>
				<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

					<td class="product-remove" style="border: medium hidden;">
						<div style="background: none repeat scroll 0px 0px padding-box rgb(0, 0, 0); border-radius: 100%; font-size: 0.757em; height: 1.48em; position: relative; display: inline-block; width: 1.5em; line-height: 1; color: rgb(255, 255, 255); text-align: center;"><?php
							echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a href="%s" class="remove" title="%s">&times;</a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), __( 'Remove this item', 'woocommerce' ) ), $cart_item_key );
						?>
						</div>
					</td>

					<td class="product-thumbnail" style="border: medium hidden;">
						<?php
							$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

							if ( ! $_product->is_visible() )
								echo $thumbnail;
							else
								printf( '<a href="%s">%s</a>', $_product->get_permalink(), $thumbnail );
						?>
					</td>

					<th class="product-name" style="border: medium hidden;color: #bcbec0;font-family: futurastdbook;">
						<?php
							if ( ! $_product->is_visible() )
								echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key );
							else
								echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', $_product->get_permalink(), $_product->get_title() ), $cart_item, $cart_item_key );

							// Meta data
							echo WC()->cart->get_item_data( $cart_item );

               				// Backorder notification
               				if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) )
               					echo '<p class="backorder_notification">' . __( 'Available on backorder', 'woocommerce' ) . '</p>';
						?>
					</th>

					<td class="product-price" style="border: medium hidden;color: #b2b2b2;">
						<?php
							echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
						?>
					</td>

					<td class="product-quantity" style="border: medium hidden;color: #b2b2b2;">1
					<input type="hidden" name="cart[<?php echo $cart_item_key; ?>][qty]" value="1" />
						<?php
							/* if ( $_product->is_sold_individually() ) {
								$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
							} else {
								$product_quantity = woocommerce_quantity_input( array(
									'input_name'  => "cart[{$cart_item_key}][qty]",
									'input_value' => $cart_item['quantity'],
									'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
								), $_product, false );
							}

							echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key ); */
						?>
					</td>

					<td class="product-subtotal" style="border: medium hidden;color: #b2b2b2;">
						<?php
							echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
						?>
					</td>
				</tr>
				<?php
			}
		}

		do_action( 'woocommerce_cart_contents' );
		?>
		<tr>
			<td colspan="6" class="actions" style="border: medium hidden;">

				<?php if ( WC()->cart->coupons_enabled() ) { ?>
					<div class="coupon">

						<label for="coupon_code"><?php _e( 'Coupon', 'woocommerce' ); ?>:</label> <input type="text" name="coupon_code" style = "line-height: 1.618em;" class = "aspk_coupon_code" id="coupon_code" value="" placeholder="<?php _e( 'Coupon code', 'woocommerce' ); ?>" /> <input style = "background: none repeat scroll 0 0 padding-box #364245;
    border: 0 none;
    border-radius: 0.3em;
    box-shadow: none;
    box-sizing: border-box;
    color: #f8f8f8;
    cursor: pointer;
    display: inline-block;
    font-family: inherit;
    font-size: 0.857em;
    font-weight: 700;
    line-height: 1.618em;
    margin-right: 3px;
    overflow: visible;
    padding: 0.53em 0.8em;
    text-align: center;
    text-decoration: none;
    text-shadow: none;
    text-transform: lowercase;" id="papu_cat" type="submit" class="button" name="apply_coupon" value="<?php _e( 'Apply Coupon', 'woocommerce' ); ?>" />

						<?php do_action('woocommerce_cart_coupon'); ?>

					</div>
				<?php } ?>

				<input type="submit" style = "background: none repeat scroll 0 0 padding-box #364245;
    border: 0 none;
    border-radius: 0.3em;
    box-shadow: none;
    box-sizing: border-box;
    color: #f8f8f8;
    cursor: pointer;
    display: inline-block;
    font-family: inherit;
    font-size: 0.857em;
    font-weight: 700;
    line-height: 1.618em;
    margin-right: 3px;
    overflow: visible;
    padding: 0.53em 0.8em;
    text-align: center;
    text-decoration: none;
    text-shadow: none;
    text-transform: lowercase;
    white-space: nowrap;"  class="button" name="update_cart" value="<?php _e( 'Update Cart', 'woocommerce' ); ?>" id="aspk_bili_cat" /> <input type="submit" id = "aspk_proceed" style="background: none repeat scroll 0 0 #b5b5b5;border: 0px none; border-radius: 0.3em; cursor: pointer; overflow: visible; padding: 0.53em 0.8em; text-align: center; text-decoration: none; text-shadow: none; text-transform: lowercase; white-space: nowrap; box-shadow: none; box-sizing: border-box; color: rgb(248, 248, 248); display: inline-block; font-family: inherit; font-size: 0.857em; font-weight: 700; line-height: 1.618em; margin-right: 3px;" class="checkout-button button alt wc-forward" name="proceed" value="<?php _e( 'Proceed to Checkout', 'woocommerce' ); ?>" />

				<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>

				<?php wp_nonce_field( 'woocommerce-cart' ); ?>
			</td>
		</tr>

		<?php do_action( 'woocommerce_after_cart_contents' ); ?>
	</tbody>
</table>

<?php do_action( 'woocommerce_after_cart_table' ); ?>

</form>

<div class="cart-collaterals agile_cart_bottom" >

	<?php do_action( 'woocommerce_cart_collaterals' ); ?>

	<?php woocommerce_cart_totals(); ?>

	<?php woocommerce_shipping_calculator(); ?>

</div>

<?php do_action( 'woocommerce_after_cart' ); ?>
<script>
heading = jQuery(".page_heading").html();
uper_heading = heading.toUpperCase();
jQuery(".page_heading").html(uper_heading);
jQuery(".page_heading").css('font-family','futurastdbook');
jQuery(".woocommerce-billing-fields h3").css('font-family','futurastdbook');
jQuery('.aspk_section').css('width','650px');
jQuery('.aspk_section').css('margin-left','150px');
jQuery('.shipping-calculator-button').css('text-transform','lowercase');
//jQuery('.attachment-shop_thumbnail').css('width','3.61em');
//jQuery('.attachment-shop_thumbnail').css('height','5em');
jQuery('.woocommerce-message').css('border-top-color','#666666');
jQuery('.product-thumbnail img').css('border','1px solid');
jQuery('.product-thumbnail img').css('padding','2px');
jQuery(document).ready(function(){
	jQuery(".remove").tipTip({
		defaultPosition :"top",
	});
})

</script>