<?php
/**
 * Empty cart page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

wc_print_notices();

?>

<p style = "color: #a7a9ac;" class="cart-empty"><?php _e( 'Your cart is currently empty.', 'woocommerce' ) ?></p>

<?php do_action( 'woocommerce_cart_is_empty' ); ?>

<p class="return-to-shop"><a style="background: none repeat scroll 0 0 padding-box #364245; border: 0 none; border-radius: 0.3em; box-shadow: none; box-sizing: border-box; color: #f8f8f8; cursor: pointer; display: inline-block; font-family: inherit; font-size: 0.857em; font-weight: 700; line-height: 1.618em; margin-right: 3px; overflow: visible; padding: 0.53em 0.8em; text-align: center; text-decoration: none; text-shadow: none; text-transform: lowercase; white-space: nowrap; width: auto;letter-spacing: 1.3px;"class="button" href="<?php echo apply_filters( 'woocommerce_return_to_shop_redirect', get_permalink( wc_get_page_id( 'shop' ) ) ); ?>"><?php _e( 'Return To Shop', 'woocommerce' ) ?></a></p>
<script>
heading = jQuery(".page_heading").html();
uper_heading = heading.toUpperCase();
jQuery(".page_heading").html(uper_heading);
jQuery('.woocommerce-message').css('border-top-color','#666666');
</script>