<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */

	// Options Framework (https://github.com/devinsays/options-framework-plugin)
	if ( !function_exists( 'optionsframework_init' ) ) {
		define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/_/inc/' );
		require_once dirname( __FILE__ ) . '/_/inc/options-framework.php';
	}

	// Theme Setup (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	if ( ! function_exists( 'new_olx_theme_setup' ) ) {
		function new_olx_theme_setup() {
			load_theme_textdomain( 'new_olx', get_template_directory() . '/languages' );
			add_theme_support( 'automatic-feed-links' );
			add_theme_support( 'structured-post-formats', array( 'link', 'video' ) );
			add_theme_support( 'post-formats', array( 'aside', 'audio', 'chat', 'gallery', 'image', 'quote', 'status' ) );
			register_nav_menu( 'primary', __( 'Navigation Menu', 'html5reset' ) );
			add_theme_support( 'post-thumbnails' );
			register_nav_menus( array(
				'primary'=> __( 'Navigation Menu', 'new_olx' ),
				'menu-footer' => __( 'Footer Menu', 'new_olx' ),
				'top_bar_nav' => __( 'Top bar Menu2', 'new_olx' ),
				'my_account' => __( 'My Account Menu', 'new_olx' ),
				//'footer-menu-info' => __( 'Footer-Menu-Info', 'new_olx' ),
				'aspk_header' => __( 'Main Header Menu', 'new_olx' ),
				'account_log' => 'Account LOG',
				'super2' => 'Super Menu2'
			) );

		}
	}
	add_action( 'after_setup_theme', 'new_olx_theme_setup' );

	// Scripts & Styles (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	function html5reset_scripts_styles() {
		global $wp_styles;

		// Load Comments
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
			wp_enqueue_script( 'comment-reply' );

		// Load Stylesheets
//		wp_enqueue_style( 'html5reset-reset', get_template_directory_uri() . '/reset.css' );
//		wp_enqueue_style( 'html5reset-style', get_stylesheet_uri() );

		// Load IE Stylesheet.
//		wp_enqueue_style( 'html5reset-ie', get_template_directory_uri() . '/css/ie.css', array( 'html5reset-style' ), '20130213' );
//		$wp_styles->add_data( 'html5reset-ie', 'conditional', 'lt IE 9' );

		// Modernizr
		// This is an un-minified, complete version of Modernizr. Before you move to production, you should generate a custom build that only has the detects you need.
		// wp_enqueue_script( 'html5reset-modernizr', get_template_directory_uri() . '/_/js/modernizr-2.6.2.dev.js' );

	}
	add_action( 'wp_enqueue_scripts', 'html5reset_scripts_styles' );

	// WP Title (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	function html5reset_wp_title( $title, $sep ) {
		global $paged, $page;

		if ( is_feed() )
			return $title;

//		 Add the site name.
		$title .= get_bloginfo( 'name' );

//		 Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title = "$title $sep $site_description";

//		 Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 )
			$title = "$title $sep " . sprintf( __( 'Page %s', 'html5reset' ), max( $paged, $page ) );

		return $title;
	}
	add_filter( 'wp_title', 'html5reset_wp_title', 10, 2 );




//OLD STUFF BELOW


	// Load jQuery
	if ( !function_exists( 'core_mods' ) ) {
		function core_mods() {
			if ( !is_admin() ) {
				wp_enqueue_script( 'jquery' );
			}
		}
		add_action( 'wp_enqueue_scripts', 'core_mods' );
	}

	// Clean up the <head>, if you so desire.
	//	function removeHeadLinks() {
	//    	remove_action('wp_head', 'rsd_link');
	//    	remove_action('wp_head', 'wlwmanifest_link');
	//    }
	//    add_action('init', 'removeHeadLinks');

	// Custom Menu
	//register_nav_menu( 'primary', __( 'Navigation Menu', 'html5reset' ) );
	
	
	/* register_nav_menus( array(
		'primary'=> __( 'Navigation Menu', 'html5reset' ),
		'menu-footer' => __( 'Footer Menu', 'new_olx' ),
		'top_bar_nav' => __( 'Top bar Menu2', 'new_olx' ),
		'my_account' => __( 'My Account Menu', 'new_olx' ),
		//'footer-menu-info' => __( 'Footer-Menu-Info', 'new_olx' ),
		'aspk_header' => __( 'Main Header Menu', 'new_olx' ),
	) ); */

	// Widgets
	if ( function_exists('register_sidebar' )) {
		function html5reset_widgets_init() {
			register_sidebar( array(
				'name'          => __( 'Sidebar Widgets', 'html5reset' ),
				'id'            => 'sidebar-primary',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );
			register_sidebar( array(
				'name'          => __( 'Product Sidebar (4 column)', 'New-olx' ),
				'id'            => 'product_sidebar',
				'before_widget' => '<div >',
				'after_widget'  => '</div>',
				'before_title'  => '<span id="romove_cart_title">',
				'after_title'   => '</span>',
			) );
			register_sidebar( array(
				'name'          => __( 'Header Search (4 column)', 'New-olx' ),
				'id'            => 'header-search',
				'before_widget' => '<div >',
				'after_widget'  => '</div>',
				'before_title'  => '',
				'after_title'   => '',
			) );
			register_sidebar( array(
				'name'          => __( 'Footer Menu(4 column)', 'New-olx' ),
				'id'            => 'footer_menu',
				'before_widget' => '<div >',
				'after_widget'  => '</div>',
				'before_title'  => '',
				'after_title'   => '',
			) );
		}
		add_action( 'widgets_init', 'html5reset_widgets_init' );
	}

	// Navigation - update coming from twentythirteen
	function post_navigation() {
		echo '<div class="navigation">';
		echo '	<div class="next-posts">'.get_next_posts_link('&laquo; Older Entries').'</div>';
		echo '	<div class="prev-posts">'.get_previous_posts_link('Newer Entries &raquo;').'</div>';
		echo '</div>';
	}

	// Posted On
	function posted_on() {
		printf( __( '<span class="sep">Posted </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a> by <span class="byline author vcard">%5$s</span>', '' ),
			esc_url( get_permalink() ),
			esc_attr( get_the_time() ),
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_author() )
		);
	}

	//login
	function aspk_check_login(){
		if ( is_user_logged_in() ) {
			//echo 'Welcome, registered user!';
		} else {
			aspk_show_login_screen();
		}
		
		
	}
	add_action('init', 'aspk_fe_init');
	function aspk_fe_init(){
		ob_start();
	}
	function aspk_show_login_screen(){
		global $agile_bt;
		if(isset($_POST['aspk_signup'])){
			$user_id = $agile_bt->aspk_handle_signup();
			if(is_numeric($user_id)){
				wp_set_auth_cookie($user_id);
				wp_redirect(home_url());
				exit;
			}else{
				echo $user_id->get_error_message();
			}
		}elseif(isset($_POST['aspk_signin'])){
			$user_id = $agile_bt->aspk_handle_signin();
			wp_redirect(home_url());
			exit;
		}elseif(isset($_POST['aspk_forget_form'])){
			$user_id = $agile_bt->aspk_handle_forgot();
			wp_redirect(home_url());
			exit;
		}
		?>
			<div id="aspk_freeze" class="freeze"></div>
			<div  style="position: absolute; left: 50%;">
				<div id="aspk_login_box">
					<form  method="post" action="" id="aspk_signup_form" style="display:none;">
						<div style="padding: 41px 37px 23px 37px;">
							<div  class="heading-popup">
								<h3 class="aspk_heading-popup" style="font-size:27px;text-align:center;font-family:'DidotLTStdItalic13774';color:#fff;">
									Welcome back
								</h3>
							</div>
							<div  class="2nd-heading-popup">
								<h2 style="font-size: 33px;text-align: center;text-transform: uppercase !important;font-family: 'FuturaStdBold';color: #ffffff;letter-spacing: 2px;padding: 5px 0 0;line-height: 6px;">sign up</h2>
								<p style="margin: 0;font-family: 'FuturaStdMedium';font-size: 14px;color:#fff;text-align:center;text-transform: uppercase;padding-bottom: 15px;padding-top: 15px;">for exclusive access to <br> previously owned by a gay man</p>
								
							</div>
								<div style="clear:both;text-align:center">
									<div style="clear:left;font-family: 'FuturaStdMedium';font-size: 17px;color: yellow;text-align: center;" id="aspk_msg"></div>
									<div style="clear:left;font-family: 'FuturaStdMedium';font-size: 17px;color: yellow;text-align: center;" id="agile_singnup_fail">
									</div>
									<div style="clear:left;">
										<input required type="email" id="aspk_filed_email" placeholder="EMAIL ADDRESS" name="email-adr" class="aspk_span_filed" >
										<input required style="display:none;" class="aspk_span_filed" id="aspk_filed_pwd" type="password" placeholder="PASSWORD" name="pwd">
									</div>
									<div style="clear:left;">
										<input type="button" onclick="aspk_validtion_form();" value="join" name="submit_email" class="aspk_newsletter_submit" id="aspk_submit_email"> 
										<input style="display:none;" id="aspk_filed_signup" class="aspk_newsletter_submit"  type="submit" value="submit" name="aspk_signup">
									</div>
								</div>
								<div style="clear:both;display:none;border-top: 1px solid;color:white;" id="div_singin">
									<p style="margin: 0;font-family: 'DidotLTStdItalic13774';font-size: 18px;color:#fff;margin-top: 20px;text-align: center;" >Already a member? <a href="#" id="signin_link" style="font-size: 18px;color:#fff !important;padding: 0 0 0 4px;font-family: 'FuturaStdBold';" onclick="aspk_signin_click()">SIGN IN</p></a>
								</div>
						</div>
					</form> 
					<form  method="post" action="" id="aspk_signin_form">
						<div style="padding: 41px 37px 23px 37px;">
							<div  class="heading-popup">
								<h3 class="aspk_heading-popup" style="font-size:27px;text-align:center;font-family:'DidotLTStdItalic13774';color:#fff;">
									Welcome back
								</h3>
							</div>
							<div  class="2nd-heading-popup">
								<h2 style="font-size: 33px;text-align: center;text-transform: uppercase !important;font-family: 'FuturaStdBold';color: #ffffff;letter-spacing: 2px;padding: 10px 0 0;line-height: 6px;">sign IN</h2>
								<p style="margin: 0;font-family: 'FuturaStdMedium';font-size: 19px;color:#fff;text-align:center;padding: 10px;">To previously<br> owned by a gay man</p>
								
							</div>
								<div style="clear:both;text-align:center">
									<div style="clear:left;font-family: 'FuturaStdMedium';font-size: 14px;color: yellow;text-align: center;display:none;padding-bottom: 5px;" id="aspk_singin_email_msg">
										Email address/password incorrect. <br/>Please try again.
									</div>
									<div style="clear:left;">
										<input id="agile_email_field" type="email"  placeholder="EMAIL ADDRESS" name="email-adr" class="aspk_span_filed" >
									</div>
									<div style="clear:left;margin-top:1em;">
										<input id="agile_pasword" class="aspk_span_filed" id="aspk_filed_pwd" type="password" placeholder="PASSWORD" name="pwd">
									</div>
									<div style="clear:left;">
										<input  id="aspk_filed_signin" class="aspk_newsletter_submit" type="button" value="submit" onclick="agile_submit_sigin()" name="aspk_signin">
									</div>
									<div style="clear:left;">
										<a style="letter-spacing: 2px;text-transform: capitalize; color:white !important;font-family: 'DidotLTStdItalic13774';font-size:18px;"href="#" id="forgot" class="forgot_pass" onclick="forgot_pwd_click()">forgot password?</a>
									</div>
									<div style="clear:left;">
										<?php echo $agile_bt->fb_login_integration(); ?>
									</div>
								</div>
								<div style="margin-top: 10px;clear:both;border-top: 1px solid;color:white;" id="div_singup">
									<p style="margin: 0;font-family: 'DidotLTStdItalic13774';font-size: 18px;color:#fff;margin-top: 20px;text-align: center;" >Not a member?<a href="#" style="font-size: 20px;color:#fff !important;padding: 0 0 0 4px;font-family: 'FuturaStdBold';" id="signin_link" onclick="aspk_signup_click()">SIGN UP</p></a>
								</div>
						</div>
					</form> 
					<form  method="post" action="" id="aspk_forget_psw_form" style="display:none;">
						<div style="padding: 41px 37px 23px 37px;">
							<div  class="heading-popup">
								<h3 class="aspk_heading-popup" style="font-size:27px;text-align:center;font-family:'DidotLTStdItalic13774';color:#fff;">
									Welcome back
								</h3>
							</div>
							<div  class="2nd-heading-popup">
								<h2 style="font-size: 33px;text-align: center;text-transform: uppercase !important;font-family: 'FuturaStdBold';color: #ffffff;letter-spacing: 2px;padding: 24px 0 0;margin: 0 0 5px;line-height: 46px;">forgot password</h2>
								
							</div>
								<div style="clear:both;text-align:center">
									<div style="clear:left;font-family: 'FuturaStdMedium';font-size: 14px;color: yellow;text-align: center;display:none;" id="aspk_forget_msg">
											There is no user registered with this email address.
									</div>
									<div style="clear:left;font-family: 'FuturaStdMedium';font-size: 14px;color: yellow;text-align: center;display:none;" id="aspk_forget_msg_true">
											Check your email for the new password.
									</div>
									<div style="clear:left;">
										<input id="aspk_f_email" type="email"  placeholder="EMAIL ADDRESS" name="email-adr" class="aspk_span_filed" >
									</div>
									<div style="clear:left;">
										<input  id="aspk_filed_signin" class="aspk_newsletter_submit" type="button" onclick="aspk_forget_paswrod_mail()" value="submit" name="aspk_forget_form">
									</div>
								</div>
								<div style="margin-top: 10px;clear:both;border-top: 1px solid;color:white;" id="div_singup">
									<p style="margin: 0;font-family: 'DidotLTStdItalic13774';font-size: 18px;color:#fff;margin-top: 20px;text-align: center;" >Not a member?<a href="#" style="font-size: 20px;color:#fff !important;padding: 0 0 0 4px;font-family: 'FuturaStdBold';" id="signin_link" onclick="aspk_signup_click()">SIGN UP</p></a>
								</div>
						</div>
					</form> 
				</div>
			</div>
			<script>
					function aspk_validtion_form(){
						var x = jQuery('#aspk_filed_email').val();
							var atpos = x.indexOf("@");
							var dotpos = x.lastIndexOf(".");
							if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=x.length) {
								jQuery("#aspk_msg").html("Please enter valid email");
								return false;
							}
						jQuery("#aspk_msg").show();
						aspk_check_email_exist();
					}
					function aspk_check_email_exist(){
						jQuery("#agile_singnup_fail").html('');
						var aspk_email =   jQuery("#aspk_filed_email").val();
							var data = {
								'action': 'aspk_check_email',
								'email-adr': aspk_email      
							};
							var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
							jQuery.post(ajaxurl, data, function(response) {
								jQuery("#aspk_fd_subcat").html(response);
								obj = JSON.parse(response);
								 if(obj.st == 'ok'){	
									jQuery("#aspk_msg").hide();
									jQuery("#agile_singnup_fail").html('');
									jQuery('#aspk_submit_email').hide();
									jQuery('#aspk_filed_signup').show();
									jQuery('#aspk_filed_pwd').show();
									jQuery('#aspk_filed_email').hide();
								 }
								 if(obj.st == 'fail'){
									 jQuery("#agile_singnup_fail").html(obj.msg);
									 jQuery('#aspk_filed_signup').hide();
									 jQuery('#aspk_filed_email').show();
									 jQuery('#aspk_filed_pwd').hide();
									 jQuery('#aspk_submit_email').show();
									 jQuery("#aspk_msg").hide();
								 }

							});
					}
					
					function aspk_signin_click(){
						jQuery("#agile_singnup_fail").html('');
						jQuery('#aspk_signin_form').show();
						jQuery('#aspk_signup_form').hide();
						jQuery('#div_singin').hide();
						jQuery('#aspk_forget_psw_form').hide();
						jQuery('#div_singup').show();
					}
					function aspk_signup_click(){
						jQuery("#agile_singnup_fail").html('');
						jQuery('#aspk_signin_form').hide();
						jQuery('#aspk_signup_form').show();
						jQuery('#div_singin').show();
						jQuery('#div_singup').hide();
						jQuery('#aspk_forget_psw_form').hide();
					}
					function forgot_pwd_click(){
						jQuery("#agile_singnup_fail").html('');
						jQuery('#aspk_signin_form').hide();
						jQuery('#aspk_signup_form').hide();
						jQuery('#aspk_forget_psw_form').show();
					}
					
					function aspk_forget_paswrod_mail(){
						var aspk_email =   jQuery("#aspk_f_email").val();
							var data = {
								'action': 'aspk_forget',
								'email-adr': aspk_email      
							};
							var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
							jQuery.post(ajaxurl, data, function(response) {
								jQuery("#aspk_fd_subcat").html(response);
								obj = JSON.parse(response);
								 if(obj.msg == 'User Not Exist.'){
									 jQuery("#aspk_forget_msg").show();
									  jQuery("#aspk_forget_msg_true").hide();
								 }
								 if(obj.msg == 'Mail Sent.'){
									 jQuery("#aspk_forget_msg_true").show();
									  jQuery("#aspk_forget_msg").hide();
								 }

							});
					}
					function agile_submit_sigin(){
						var aspk_email =   jQuery("#agile_email_field").val();
						var aspk_paswd =   jQuery("#agile_pasword").val();
							var data = {
								'action': 'aspk_sign_now',
								'pwd': aspk_paswd ,
								'email-adr': aspk_email      
							};
							var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
							jQuery.post(ajaxurl, data, function(response) {
								jQuery("#aspk_fd_subcat").html(response);
								obj = JSON.parse(response);
								 if(obj.st == 'ok'){	
									window.location.href="<?php echo home_url() ; ?>"
								 }
								 if(obj.st == 'fail'){
									 jQuery("#aspk_singin_email_msg").show();
								 }

							});
					}
					
			</script>

		<?php
	}
	