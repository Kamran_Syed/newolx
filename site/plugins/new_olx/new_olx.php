<?php
/*
Plugin Name: Agile New OLX
Plugin URI: 
Description: Specially created to work with New OLX theme. It creates the desired end result
Author: Agile Solutions PK
Version: 1.1
Author URI: http://agilesolutionspk.com
*/

if ( !class_exists( 'Agile_New_OLX' )){

	class Agile_New_OLX{
		
		function __construct(){
			add_shortcode( 'aspk_shop', array(&$this, 'aspk_shop') );
			add_action('wp_footer', array(&$this,'front_end' ));
			add_shortcode( 'seller_form', array(&$this, 'seller_form') );
			add_shortcode( 'email_verification_sc', array(&$this, 'email_verification_sc') );
			register_activation_hook( __FILE__, array($this, 'install') );
			add_action( 'admin_menu', array(&$this, 'admin_menu') );
			add_action( 'woocommerce_after_my_account', array(&$this, 'add_accout_detail'),999 );
			add_action('wp_enqueue_scripts', array(&$this, 'wp_enqueue_scripts'));
			add_action( 'wp_ajax_nopriv_aspk_forget', array(&$this,'aspk_handle_forgot' ));
			add_action( 'wp_ajax_nopriv_aspk_sign_now', array(&$this,'aspk_handle_signin' ));
			//add_action( 'wp_ajax_nopriv_aspk_sign_up', array(&$this,'aspk_handle_signup' ));
			add_action( 'wp_ajax_nopriv_aspk_check_email', array(&$this,'aspk_check_email_exist' ));
			add_action( 'wp_ajax_nopriv_aspk_singup_f_b', array(&$this,'aspk_signup_ussing_f_b' ));
		}
		
		function aspk_signup_ussing_f_b(){
			$email = $_POST['email-adr'];
			$first_name = $_POST['first_name'];
			$check_user = get_user_by( 'email', $email );
			if($check_user == false){
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
			$random_password = substr( str_shuffle( $chars ), 0, $length );
			$user_id = wp_create_user( $first_name, $random_password, $email );
			
			wp_set_auth_cookie($user_id);
			wp_redirect(home_url());
			exit;
			} else {
				wp_redirect(home_url());
			}
		}
		
		function wp_enqueue_scripts(){	
			wp_enqueue_style( 'aspk-tootip-css', plugins_url('css/tiptip.css', __FILE__) );
			wp_enqueue_style( 'font-awesome', plugins_url('font-awesome/css/font-awesome.min.css', __FILE__) );
		}
		
		function aspk_check_email_exist(){
			$email = $_POST['email-adr'];
			$check_user = get_user_by( 'email', $email );
			if($check_user == false){
				$ret = array('act' => 'check_user','st'=>'ok','msg'=>'user not exist');
				echo json_encode($ret);
			}else{
				$ret = array('act' => 'check_user','st'=>'fail','msg'=>'This email is already registered. Please access SIGN IN link below.');
				echo json_encode($ret);
			}
			exit;
		}
	
		function aspk_handle_signup(){
			global $woocommerce;
			alog('$_POST',$_POST,__FILE__,__LINE__);
			
			$email = $_POST['email-adr'];
			$password = $_POST['pwd'];
			$user_id = wc_create_new_customer( $email, $username = '', $password );// return user id int
			return $user_id;
			
			
		}
		
		function add_accout_detail(){
			?>
			<h2 style="font-size: 1.387em;  color: #a7a9ac;">My saved cards</h2>
			<div class="row" style = "clear:left;">
				<div class="col-md-12" style="margin-top:1em;color: #a7a9ac; width: 24em;font-size: 85%;">You do not have any saved cards.</div>
			</div>
			<?php
		}
		
		function install(){
			global $wpdb;
			
			$sql="
				CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."email_verification` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `mail` varchar(255) NOT NULL,
				  `rand_no` varchar(30) NOT NULL,
				  `verified` varchar(30) NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
			";
			$wpdb->query($sql);
	
		}
		
		function email_verification_sc(){
		
			if(isset( $_GET['sec_key'])){
				$sec_key = $_GET['sec_key'];
			}
				$decode = base64_decode($sec_key);
				$row_id = $this->get_key_byid($decode);
				$this->agile_select_data();
				$this->update_verified($row_id);
				
			if(isset( $_GET['postid'])){
				$pid = $_GET['postid'];
				$this->send_admin_mail_publish_post($pid);
				echo "Your product has verified and approval is Pending From Admin.";
			}
						
			
		}
		
		function admin_menu(){
			add_menu_page('Verify Email', 'Verify Email', 'manage_options', 'agile_email_verification', array(&$this, 'email_verification') );
			add_submenu_page('agile_email_verification', 'Select Data', 'Select Data', 'manage_options', 'agile_select_data', array(&$this, 'agile_select_data'));
		}
		
		function agile_select_data(){
			global $wpdb;
			
			//$r_no = $_GET['rand_no'];
			$r_no = $this->get_email_data();
			$sql = "SELECT `mail` FROM {$wpdb->prefix}email_verification WHERE rand_no = {$r_no}";
			$wpdb->get_row($sql);
		}
		
		function get_key_byid($decode){
		
			global $wpdb;
			$sql = "SELECT `id` FROM {$wpdb->prefix}email_verification WHERE rand_no = '{$decode}'";
			$rs = $wpdb->get_var($sql);
			return $rs;
		}
		
		
		function update_verified($row_id){
			global $wpdb;
			if($row_id){
			  $sql = "Update {$wpdb->prefix}email_verification set verified = 'verified' WHERE id = {$row_id}";
				$result = $wpdb->query($sql);
			}
		
		}
		
		function send_admin_mail_publish_post($pid){
		
			$admin_email = get_option('admin_email');
			$aurl = admin_url( 'post.php?post='.$pid.'&action=edit');
			$encoded_url = urlencode ($aurl);
			alog('$encode_url',$encoded_url,__FILE__,__LINE__);
			$url = home_url( 'wp-login.php?redirect_to='.$encoded_url);
			$msg = '<div style="font-weight: bold;"> The Product is Pending For Approval Please Publish That Product.</div><div> <a href ='.$url.'><button style="margin-top: 1em; cursor: auto;"> Publish </button></a></div>'; 
			$headers = 'From: newolx@shop.com <newolx@shop.com>' . "\r\n";
			add_filter( 'wp_mail_content_type',array(&$this,'content_type' ));
			wp_mail($admin_email,'product Verification Email',$msg,$headers);
			remove_filter( 'wp_mail_content_type', array(&$this,'content_type' ));
		}
		
		function content_type($content_type ){
			return 'text/html';
		}
		
		
		/*function avoid_dupliaction_email($email){
			global $wpdb;
			$dup_email = "SELECT `mail` FROM {$wpdb->prefix}email_verification WHERE mail = '{$email}'";
			$existmail = $wpdb->get_var($dup_email);
			return $existmail;
		}*/
		
		function insert_unverified_data_create_cutomer_product($email ,$security_key ){
			global $wpdb;
			
			$sql = "INSERT INTO {$wpdb->prefix}email_verification (mail,rand_no,verified) VALUES('{$email}','{$security_key}' , 'unverified')";
			$wpdb->query($sql);
			
		}
		
		function get_email_data(){
			$email = 'haiderbilal58@gmail.com';
			//$random_no = rand(0,99);
			$random_no = 92;
			//$this->insert_unverified_data_create_cutomer_product($email ,$random_no );
			return $random_no;
		}
		
		function email_verification(){
			$this->get_email_data();	
		}
		
		function random_password( $length = 8 ) { //generates random password.
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;?";
			$password = substr( str_shuffle( $chars ), 0, $length );
			return $password;
		}
		
		function add_term_relationship($tid,$post_id){
			global $wpdb;
			
			$term = 0;
			$sql = "insert into {$wpdb->prefix}term_relationships ( object_id, term_taxonomy_id,term_order) values({$post_id} , {$tid} , {$term} )";
			$wpdb->query($sql);
		}
		
		function aspk_handle_signin(){
			$user_email = explode("@", $_POST['email-adr']);
			$creds = array();
			//$creds['user_login'] = $_POST['email-adr'];
			$creds['user_login'] = $user_email[0];
			$creds['user_password'] = $_POST['pwd'];
			$creds['remember'] = true;
			$user = wp_signon( $creds, false );
			if(is_wp_error( $user )){
				$ret = array('act' => 'sign_now','st'=>'fail','msg'=>$user->get_error_message());
				echo json_encode($ret);
				exit;
			}else{
				$ret = array('act' => 'sign_now','st'=>'ok','msg'=>'user_logged_in');
				echo json_encode($ret);
				exit;
			}
		}
		
		function aspk_handle_forgot(){
			$email = $_POST['email-adr'];
			$user = get_user_by( 'email', $email );
			if(empty($user)){
				$ret = array('act' => 'forgot_password','st'=>'fail','msg'=>'User Not Exist.');
				echo json_encode($ret);
				exit;
			}else{ 
				$user_id = $user->ID;
				$password = $this->random_password();
				alog('$password',$password,__FILE__,__LINE__);
				$set_password = wp_set_password( $password, $user_id );
				$link_mail_genrate = '<div style="font-weight: bold;width:25em;">Your New Password is.</div><div> Password is '.$password.'</div>'; 
				add_filter( 'wp_mail_content_type',array(&$this,'wp_set_content_type' ));
				$send_mail = wp_mail( $email, 'Reset Password', $link_mail_genrate );
				remove_filter( 'wp_mail_content_type', array(&$this,'wp_set_content_type' ));
				if($send_mail == true){
					$ret = array('act' => 'forgot_password','st'=>'ok','msg'=>'Mail Sent.');
					echo json_encode($ret);
					exit;
				}else{
					$ret = array('act' => 'forgot_password','st'=>'fail','msg'=>'Mail Not Sent.');
					echo json_encode($ret);
					exit;
				}
			}
		}
		
		function create_product($user_id,$item_head,$products_cat,$esti_price,$list_price,$item_desc,$condition,$length,$width,$height,$other_dim,$weight,$img_files,$street_add,$city,$countries,$post_code,$phn_num,$receive_updates,$agree_terms_conditions,$password,$username,$email){
			$post = array(
				'post_author' => $user_id,
				'post_content' => '',
				'post_status' => "draft",
				'post_title' => $item_head,
				'post_parent' => '',
				'post_type' => "product",
			);
			//Create post
			$post_id = wp_insert_post( $post );
			$post_thumbnail_id = get_post_thumbnail_id( $post_id );
			if($post_id){
				$n = 0; 
				foreach($img_files as $movefile){
					$n ++;
					if($n == 1){
						$attachment = array(
							'guid'           => $movefile['url'] . '/' . basename( $movefile['file'] ), 
							'post_mime_type' => $movefile['type'],
							'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $movefile['file'] ) ),
							'post_content'   => '',
							'post_status'    => 'inherit'
						);

						$attach_id = wp_insert_attachment( $attachment, $movefile['file'], $post_id );
						if($attach_id)	add_post_meta($post_id, '_thumbnail_id', $attach_id);
					}else{
						$attachment_gallery = array(
							'guid'           => $movefile['url'] . '/' . basename( $movefile['file'] ), 
							'post_mime_type' => $movefile['type'],
							'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $movefile['file'] ) ),
							'post_content'   => '',
							'post_status'    => 'inherit'
						);

						$gallery_attach_id = wp_insert_attachment( $attachment_gallery, $movefile['file'], $post_id );
						$gallery_attach_ids[] = $gallery_attach_id;
					}
					
				}
				if(! empty($gallery_attach_ids) ){
					$gallery_attach_ids = implode(',',$gallery_attach_ids);
					if($gallery_attach_id)	update_post_meta($post_id, '_product_image_gallery', $gallery_attach_ids);
				}
				$this->add_term_relationship($products_cat,$post_id);
				update_post_meta( $post_id, '_regular_price', $esti_price );
				update_post_meta( $post_id, '_sale_price', $list_price);
				update_post_meta( $post_id, '_purchase_note', $item_desc );
				update_post_meta( $post_id, '_weight', $weight );
				update_post_meta( $post_id, '_length', $length );
				update_post_meta( $post_id, '_width', $width );
				update_post_meta( $post_id, '_height', $height);
				update_post_meta( $post_id, '_product_attributes', array());
				update_post_meta( $post_id, '_price', $list_price );
				update_post_meta( $post_id, '_other_dimension', $other_dim );
				update_post_meta( $post_id, '_aspk_condition', $condition );
				$this->add_post_desc($post_id, $item_desc);
				$security_key = $this->random_password();
				$this->send_admin_mail_publish_post($post_id);
				/* if(is_user_logged_in()){
					$this->send_admin_mail_publish_post($post_id);
				}elseif(is_wp_error( $user_id )){
					$this->insert_unverified_data_create_cutomer_product($email ,$security_key );
					$this->send_mail_client_exist($email,$security_key,$post_id);
				}else{
					$this->insert_unverified_data_create_cutomer_product($email ,$security_key );
					$this->send_mail_client($email,$security_key,$post_id, $password,$username);
					//$this->change_post_status($post_id);
				} */
				?>
				<div class="row" style="clear:left;">
				   <div class="col-md-3" style="margin-top:1em;"></div>
					<div class="col-md-3" style="margin-top:1em; font-size: initial; font-weight:bold; background-color: #f7f7f7;">Thank you For Submission
					</div>
					<div class="col-md-3" style="margin-top:1em;"></div>
				</div>
				<?php
				
			}else{
			 // error handling product not add 
			}
			
		}
		
		function add_post_desc($post_id, $post_desc){
		
			$my_post = array(
				  'ID'           => $post_id,
				  //'post_excerpt' => $post_desc
				  'post_content' => $post_desc
			  );

			wp_update_post($my_post);
		}
		
		function send_mail_client($email,$security_key,$post_id,$password,$username){ 
		
			$page = get_page_by_title( 'Verification Email' );
			$link = get_permalink( $page->ID );	
			$encode = base64_encode($security_key);
			$link = $link.'?sec_key='.$encode.'&postid='.$post_id;
			$headers = 'From: newolx@shop.com <newolx@shop.com>' . "\r\n";
			$link_mail_genrate = '<div style="font-weight: bold;">Your Product Has Verified and Approval is Pending From Admin.</div><div><a href ="'.$link.'" ><button style="margin-top: 1em; cursor: auto;">Verified</button></a><br><br><div style="font-weight: bold;">Your Account Details are:<br> Username = '.$username.'<br> Password = '.$password.'</div></div>'; 
			add_filter( 'wp_mail_content_type',array(&$this,'wp_set_content_type' ));
			$send_mail = wp_mail( $email, 'Verification Email', $link_mail_genrate ,$headers );
			remove_filter( 'wp_mail_content_type', array(&$this,'wp_set_content_type' ));
			if($send_mail == true){
				?>
				<div class="row" style="clear:left;">
				   <div class="col-md-3" style="margin-top:1em;"></div>
					<div class="col-md-6" style="margin-top:1em; font-size: initial; font-weight:bold; background-color: #f7f7f7;">Thank you For Submission!
					</div>
					<div class="col-md-3" style="margin-top:1em;"></div>
				</div>
				<?php
			}
			
		}
		
		function send_mail_client_exist($email,$security_key,$post_id){ 
		
			$page = get_page_by_title( 'Verification Email' );
			$link = get_permalink( $page->ID );	
			$encode = base64_encode($security_key);
			$link = $link.'?sec_key='.$encode.'&postid='.$post_id;
			$page_seller = get_page_by_title( 'SELLER’S AGREEMENT' );
		    $seller_link = get_permalink( $page_seller->ID );
			$headers = 'From: newolx@shop.com <newolx@shop.com>' . "\r\n";
			$link_mail_genrate = '<div style="font-weight: bold;">Please Verify Added Your Product And wait Approval From Admin Thanks </div><div><a href ="'.$link.'" ><button style="margin-top: 1em; cursor: auto;">Verified</button></a><br><br><div style="font-weight: bold;">You are Existing user.Please login old Username or Password</div><div style="clear:left;"><a href="'.$seller_link.'">seller agreement. </a></div>'; 
			add_filter( 'wp_mail_content_type',array(&$this,'wp_set_content_type' ));
			$send_mail = wp_mail( $email, 'Verification Email', $link_mail_genrate ,$headers );
			remove_filter( 'wp_mail_content_type', array(&$this,'wp_set_content_type' ));
			if($send_mail == true){

				?>
				<div class="row" style="clear:left;">
				   <div class="col-md-3" style="margin-top:1em;"></div>
					<div class="col-md-6" style="margin-top:1em; font-size: initial; font-weight:bold; background-color: #f7f7f7;">Thank you For Submission!
					</div>
					<div class="col-md-3" style="margin-top:1em;"></div>
				</div>
				<?php
			}
			
		}
		
		function wp_set_content_type($content_type ){
			return 'text/html';
		}
			
		function upload_file_handle(){
			if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
			$uploadedfiles = $_FILES['files'];
			$no_of_file_upload = count($uploadedfiles['name']);
				$upld_file_onebyone = array();
				$upload_file_handler_arr = array();
				
				for($x=0; $x< $no_of_file_upload; $x++){
					if(! empty($uploadedfiles['name'])){
						$upld_file_onebyone ['name'] = $uploadedfiles['name'][$x]; 
						$upld_file_onebyone ['type'] = $uploadedfiles['type'][$x]; 
						$upld_file_onebyone ['tmp_name'] = $uploadedfiles['tmp_name'][$x]; 
						$upld_file_onebyone ['error'] = $uploadedfiles['error'][$x]; 
						$upld_file_onebyone ['size'] = $uploadedfiles['size'][$x]; 
						$get_all_file_upld[] = $upld_file_onebyone;
					}
				}
			foreach($get_all_file_upld as $uploadedfile){
				if(empty($uploadedfile['name'])) continue;
				$upload_overrides = array( 'test_form' => false );
				$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
				$upload_file_handler_arr[] = $movefile;
			}
			return $upload_file_handler_arr;
		}
			
		
		/* function get_categories(){
			  $taxonomy     = 'product_cat';
			  $orderby      = 'name';  
			  $show_count   = 0;      // 1 for yes, 0 for no
			  $pad_counts   = 0;      // 1 for yes, 0 for no
			  $hierarchical = 1;      // 1 for yes, 0 for no  
			  $title        = '';  
			  $empty        = 0;
			$args = array(
			  'taxonomy'     => $taxonomy,
			  'orderby'      => $orderby,
			  'show_count'   => $show_count,
			  'pad_counts'   => $pad_counts,
			  'hierarchical' => $hierarchical,
			  'title_li'     => $title,
			  'hide_empty'   => $empty
			);
			$all_categories = get_categories( $args );
			//print_r($all_categories);
			foreach ($all_categories as $cat) {
				//print_r($cat);
				if($cat->category_parent == 0) {
					//alog('$cat',$cat,__FILE__,__LINE__);
					$category_id = $cat->term_id;
					$args2 = array(
					  'taxonomy'     => $taxonomy,
					  //'child_of'     => 0,
					  'parent'       => $category_id,
					  'orderby'      => $orderby,
					  'show_count'   => $show_count,
					  'pad_counts'   => $pad_counts,
					  'hierarchical' => $hierarchical,
					  'title_li'     => $title,
					  'hide_empty'   => $empty
					);
					$sub_cats = get_categories( $args2 );
					return $sub_cats;
					
				}     
			}
		} */
		
		function get_categories(){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}terms ORDER BY name ASC";
			$categories = $wpdb->get_results($sql); 
			return $categories;
		}
			
		function seller_form() {
			global $woocommerce;
			
			$user_id = get_current_user_id();
			$user_login = get_user_by( 'id', $user_id );
			$categories = $this->get_categories(); 
			if(isset($_POST['aspk_save_form'])){
				$email = $_POST['email_add'];
				$item_head = $_POST['item_head'];
				$username = $_POST['f_name'];
				$products_cat = $_POST['products'];
				$esti_price = $_POST['esti_price'];
				$list_price = $_POST['list_price'];
				$item_desc = $_POST['txt_area'];
				$condition = $_POST['radio_field'];
				$length = $_POST['lenght'];
				$width = $_POST['width'];
				$height = $_POST['height'];
				$other_dim = $_POST['other_dim'];
				$weight = $_POST['weight'];
				$img_files = $this->upload_file_handle();
				$street_add = $_POST['street_add'];
				$city = $_POST['city'];
				$countries = $_POST['countries'];
				$post_code = $_POST['post_code'];
				$phn_num = $_POST['phn_num'];
				$receive_updates = $_POST['receive_updates'];
				if(isset($_POST['check_field'])){
					$agree_terms_conditions = $_POST['check_field'];
				}
				$password = $this->random_password( );
				//$user_id = wc_create_new_customer( $email, $username, $password );// return user id int
				if(is_numeric($user_id)){
					update_user_meta($user_id, 'first_name',$username);
					update_user_meta($user_id, 'shipping_first_name',$username);
					update_user_meta($user_id, 'billing_first_name',$username);
					update_user_meta($user_id, 'billing_address_1',$street_add);
					update_user_meta($user_id, 'shipping_address_1',$street_add);
					update_user_meta($user_id, 'billing_city',$city);
					update_user_meta($user_id, 'shipping_city',$city);
					update_user_meta($user_id, 'billing_postcode',$post_code);
					update_user_meta($user_id, 'shipping_postcode',$post_code);
					update_user_meta($user_id, 'billing_phone',$phn_num);
					update_user_meta($user_id, 'shipping_phone',$phn_num);
					update_user_meta($user_id, 'billing_state',$countries);
					update_user_meta($user_id, 'shipping_state',$countries);
				}	
				$this->create_product($user_id,$item_head,$products_cat,$esti_price,$list_price,$item_desc,$condition,$length,$width,$height,$other_dim,$weight,$img_files,$street_add,$city,$countries,$post_code,$phn_num,$receive_updates,$agree_terms_conditions,$password,$username,$email);
			}
			
		 ?>
			<h1 style = "font-size: 1.618em;line-height: 1.387em;margin-left: 5.5em; color: #a7a9ac;">UPLOAD FORM</h1>	 
			<div class="tw-bs container" style="width:100%;margin-left: 8em;">&nbsp;&nbsp;
				<form method="post" action="" enctype="multipart/form-data">
					<div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em; color: #a7a9ac;">Category:</div>
						<div class="col-md-2" style="margin-top:1em;">
							<Select  class = "form-control" name="products" required>
							<?php foreach($categories as $cat) { ?>
								<option value = "<?php echo $cat->term_id; ?>" ><?php echo $cat->name; ?></option>
							<?php } ?>
							</select>
						</div>
						<div class="col-md-4" style="margin-top:1em;color: #a7a9ac;">(required)</div>
					</div>
					<div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em;color: #a7a9ac;">Item headline: (required)</div>
						<div class="col-md-6" style="margin-top:1em;">
							<input  class = "form-control" type="text" name="item_head" style="height:2em;"  value="" required/>
						</div>
						<div class="col-md-3" style="margin-top:1em;"></div>
					</div>
					<div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em;color: #a7a9ac;">Estimated retail price:
							(required)</div>
						<div class="col-md-6" style="margin-top:1em;">
							<input  class = "form-control" type="text" name="esti_price" style="height:2em;"  value="" required/>
						</div>
						<div class="col-md-3" style="margin-top:1em;"></div>
					</div>
					<div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em;color: #a7a9ac;">List price (amount the buyer will be charged):
								(required)</div>
						<div class="col-md-6" style="margin-top:1em;">
							<input  class = "form-control" type="text" name="list_price" style="height:2em;"  value="" required/>
						</div>
						<div class="col-md-3" style="margin-top:1em;"></div>
					</div>
					<div class ="row">
						<div class = "col-md-3"></div>
						<div class = "col-md-9" style="color: #a7a9ac;">Please include information about your item--any flaws, brand or <br/>
						manufacturer, material, color and history.</div>
					</div>
					<div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em; color: #a7a9ac;"> Item description:
								(required)</div>
						<div class="col-md-6" style="margin-top:1em;">
							<textarea cols="30" style="width: 32em;" name="txt_area" rows="8"  class = "form-control"></textarea>
						</div>
						<div class="col-md-3" style="margin-top:1em;"></div>
					</div>
						<div class="row" style = "clear:left;">
							<div class="col-md-3" style="margin-top:1em; color: #a7a9ac;">Condition:<br/>(required)</div>
							   <div class="col-md-1" style="margin-top:1em;">
									<input type="radio" value = "New - with the tags and/or in box" name="radio_field" required/>
								</div>	
								<div class="col-md-4" style="margin-top:1em; color: #a7a9ac;">New - with the tags and/or in box</div>
								<div class="col-md-4" style="margin-top:1em;"></div>
						</div>
						<div class="row" style = "clear:left;">
							<div class="col-md-3" style="margin-top:1em;"></div>
							<div class="col-md-1" style="margin-top:1em;">
								<input type="radio" value = "Almost New - as if you just bought it" name="radio_field" id=""required />
							</div>	
							<div class="col-md-4" style="margin-top:1em; color: #a7a9ac;">Almost New - as if you just bought it</div>
							<div class="col-md-4" style="margin-top:1em;"></div>
						</div>
						<div class="row" style = "clear:left;">
							<div class="col-md-3" style="margin-top:1em;"></div>
							   <div class="col-md-1" style="margin-top:1em;">
										<input type="radio" value = "Excellent - barely used, Perfect to the untrained eye" name="radio_field" name="radio_field" id="" required/>
								</div>	
								<div class="col-md-4" style="margin-top:1em; color: #a7a9ac;">Excellent - barely used, Perfect to the untrained eye</div>
								<div class="col-md-4" style="margin-top:1em;"></div>
						</div>
						<div class="row" style = "clear:left;">
							<div class="col-md-3" style="margin-top:1em;"></div>
							   <div class="col-md-1" style="margin-top:1em;">
										<input type="radio" value = "Good - slightly worn, has small flaws, but still looks great" name="radio_field" name="radio_field" id=""required />
								</div>	
								<div class="col-md-4" style="margin-top:1em; color: #a7a9ac;">Good - slightly worn, has small flaws, but still looks great</div>
								<div class="col-md-4" style="margin-top:1em;"></div>
						</div>
					
					<div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em; color: #a7a9ac;">Length:<br/>
								(required)</div>
						<div class="col-md-6" style="margin-top:1em;">
							<input  class = "form-control" type="text" name="lenght" style="height:2em;"  value="" required/>
						</div>
						<div class="col-md-3" style="margin-top:1em;"></div>
					</div>
					<div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em; color: #a7a9ac;">Width:<br/>
								(required)</div>
						<div class="col-md-6" style="margin-top:1em;">
							<input  class = "form-control" type="text" name="width" style="height:2em;"  value="" required/>
						</div>
						<div class="col-md-3" style="margin-top:1em;"></div>
					</div>
					<div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em; color: #a7a9ac;">Height:<br/>
								(required)</div>
						<div class="col-md-6" style="margin-top:1em;">
							<input  class = "form-control" type="text" name="height" style="height:2em;"  value="" required/>
						</div>
						<div class="col-md-3" style="margin-top:1em;"></div>
					</div>
					<div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em; color: #a7a9ac;">Other dimensions:</div>
						<div class="col-md-6" style="margin-top:1em;">
							<input  class = "form-control" type="text" name="other_dim" style="height:2em;"  value="" />
						</div>
						<div class="col-md-3" style="margin-top:1em;"></div>
					</div>
					<div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em; color: #a7a9ac;">Weight:<br/>
								(required)</div>
						<div class="col-md-6" style="margin-top:1em;">
							<input  class = "form-control" type="text" name="weight" style="height:2em;"  value="" required/>
						</div>
						<div class="col-md-3" style="margin-top:1em;"></div>
					</div>
					
					
				
					<div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em; color: #a7a9ac;">Image upload:</div>
						<div class="col-md-5" style="margin-top:1em;">
							<input type="file" class = "form-control" name="files[]" required>
							<input style = "margin-top: 0.5em;" class = "form-control" type="file" name="files[]"/>
							<input style = "margin-top: 0.5em;" class = "form-control" type="file" name="files[]" />
							<input style = "margin-top: 0.5em;" class = "form-control" type="file" name="files[]"/> 
							<input style = "margin-top: 0.5em;" class = "form-control" type="file" name="files[]"/>	
							<input style = "margin-top: 0.5em;" class = "form-control" type="file" name="files[]"/>
							<input style = "margin-top: 0.5em;" class = "form-control" type="file" name="files[]"/>
							<input style = "margin-top: 0.5em;" class = "form-control" type="file" name="files[]"/>
						</div>
						<div class="col-md-4" style="margin-top:1em; color: #a7a9ac;">(required)</div>
					</div>
					<div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em; color: #a7a9ac;">Name:<br/>
								(required)</div>
						<div class="col-md-6" style="margin-top:1em;">
							<input  class = "form-control" type="text" name="f_name" style="height:2em;"  value="<?php if(!empty($user_login)) echo $user_login->first_name; ?>" required/>
						</div>
						<div class="col-md-3" style="margin-top:1em;"></div>
					</div>
					<div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em; color: #a7a9ac;">Street address:<br/>
								(required)</div>
						<div class="col-md-6" style="margin-top:1em;">
							<input  class = "form-control" type="text" name="street_add" style="height:2em;"  value="<?php echo get_user_meta($user_id , 'shipping_address_1' , true);?>" required/>
						</div>
						<div class="col-md-3" style="margin-top:1em;"></div>
					</div>
				 <div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em; color: #a7a9ac;">City:<br/>
								(required)</div>
						<div class="col-md-6" style="margin-top:1em;">
							<input  class = "form-control" type="text" name="city" style="height:2em;"  value="<?php echo get_user_meta($user_id , 'shipping_city' , true);?>" required/>
						</div>
						<div class="col-md-3" style="margin-top:1em;"></div>
					</div>
					
				
					<div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em; color: #a7a9ac;">State:<br/></div>
						<div class="col-md-2" style="margin-top:1em;">
							<Select  class = "form-control" name="countries">
										<option value="Alabama">Alabama</option>
											<option value="Alaska">Alaska</option>
											<option value="American Samoa">American Samoa</option>
											<option value="Arizona">Arizona</option>
											<option value="Arkansas">Arkansas</option>
											<option value="California">California</option>
											<option value="Colorado">Colorado</option>
											<option value="Connecticut">Connecticut</option>
											<option value="Delaware">Delaware</option>
											<option value="District of Columbia">District of Columbia</option>
											<option value="Florida">Florida</option>
											<option value="Georgia">Georgia</option>
											<option value="Guam">Guam</option>
											<option value="Hawaii">Hawaii</option>
											<option value="Idaho">Idaho</option>
											<option value="Illinois">Illinois</option>
											<option value="Indiana">Indiana</option>
											<option value="Iowa">Iowa</option>
											<option value="Kansas">Kansas</option>
											<option value="Kentucky">Kentucky</option>
											<option value="Louisiana">Louisiana</option>
											<option value="Maine">Maine</option>
											<option value="Maryland">Maryland</option>
											<option value="Massachusetts">Massachusetts</option>
											<option value="Michigan">Michigan</option>
											<option value="Minnesota">Minnesota</option>
											<option value="Mississippi">Mississippi</option>
											<option value="Missouri">Missouri</option>
											<option value="Montana">Montana</option>
											<option value="Nebraska">Nebraska</option>
											<option value="Nevada">Nevada</option>
											<option value="New Hampshire">New Hampshire</option>
											<option value="New Jersey">New Jersey</option>
											<option value="New Mexico">New Mexico</option>
											<option value="New York">New York</option>
											<option value="North Carolina">North Carolina</option>
											<option value="North Dakota">North Dakota</option>
											<option value="Northern Marianas Islands">Northern Marianas Islands</option>
											<option value="Ohio">Ohio</option>
											<option value="Oklahoma">Oklahoma</option>
											<option value="Oregon">Oregon</option>
											<option value="Pennsylvania">Pennsylvania</option>
											<option value="Puerto Rico">Puerto Rico</option>
											<option value="Rhode Island">Rhode Island</option>
											<option value="South Carolina">South Carolina</option>
											<option value="South Dakota">South Dakota</option>
											<option value="Tennessee">Tennessee</option>
											<option value="Texas">Texas</option>
											<option value="Utah">Utah</option>
											<option value="Vermont">Vermont</option>
											<option value="Virginia">Virginia</option>
											<option value="Virgin Islands">Virgin Islands</option>
											<option value="Washington">Washington</option>
											<option value="West Virginia">West Virginia</option>
											<option value="Wisconsin">Wisconsin</option>
											<option value="Wyoming">Wyoming</option>
							</select>
						</div>
						<div class="col-md-6" style="margin-top:1.3em; color: #a7a9ac;">(required)</div>
					</div>
					<div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em; color: #a7a9ac;">Postal code:<br/>(required)</div>
						<div class="col-md-6" style="margin-top:1em;">
							<input  class = "form-control" type="text" name="post_code" style="height:2em;"  value="<?php echo get_user_meta($user_id , 'shipping_postcode' , true);?>" required/>
						</div>
						<div class="col-md-3" style="margin-top:1em;"></div>
					</div>
					
					<div class="row" style = "clear:left;">
						<div class="col-md-3" style="margin-top:1em; color:#a7a9ac;">Phone number:<br/>(required)</div>
						<div class="col-md-6" style="margin-top:1em;">
							<input  class = "form-control" type="text" name="phn_num" style="height:2em;"  value="<?php echo get_user_meta($user_id , 'billing_phone' , true);?>" required/>
						</div>
						<div class="col-md-3" style="margin-top:1em;"></div>
					</div>
					<div class="row" style = "clear:left;display:none;">
						<div class="col-md-3" style="margin-top:1em;color: #a7a9ac;">Email address:</div>
						<div class="col-md-6" style="margin-top:1em;">
							<input  class = "form-control" type="email" name="email_add" style="height:2em;"  value="<?php if(!empty($user_login)) echo $user_login->user_email?>" required/>
						</div><br/>
						<div class="col-md-3" style="color: #a7a9ac;">(valid email required)</div>
					</div>
					
						<div class="row" style = "clear:left;">
							<div class="col-md-3" style="margin-top:1em;"></div>
							   <div class="col-md-1" style="margin-top:1em;">
									<input type="checkbox" name="receive_updates" required/>
								</div>	
								<div class="col-md-4" style="margin-top:1em; color: #a7a9ac;">I Would like to receive updates from previously owned by a gay man.</div>
								<div class="col-md-4" style="margin-top:1em; color: #a7a9ac;"></div>
						</div>
						<div class="row" style = "clear:left;">
							<div class="col-md-3" style="margin-top:1em;"></div>
							   <div class="col-md-1" style="margin-top:1em;">
									<input  type="checkbox" name="check_field" required/>
								</div>	
								<div class="col-md-4" style="margin-top:1em; color: #a7a9ac;">I Agree to the Terms and Conditions and accept the Seller's Agreement..</div>
								<div class="col-md-4" style="margin-top:1em;"></div>
						</div>
					<div class="row" style="clear:left;">
					   <div class="col-md-3" style="margin-top:1em;"></div>
						<div class="col-md-6" style="margin-top:1em; margin-left: 3em;">
							<input type="submit"  value = "Submit" name="aspk_save_form" class="btn btn-primary" style="background: black;"></input>
						</div>
						<div class="col-md-3" style="margin-top:1em;"></div>
					</div>
					<?php if(isset($_POST['aspk_save_form'])){ ?>
					<div class="row" style="clear:left;">
					   <div class="col-md-3" style="margin-top:1em;margin-bottom:1em;"></div>
						<div class="col-md-3 style="margin-top:1em; margin-bottom:1em; font-size: initial; font-weight:bold; background-color: #f7f7f7;">Thank you For Submission
						</div>
						<div class="col-md-3" style="margin-top:1em;"></div>
					</div>
					<?php } ?>
				</form>
			</div>
			<?	
		}
		function front_end(){
			global $post;
			alog('$post',$post,__FILE__,__LINE__);
			if($post->post_type == 'page'){ ?>
				<script>
					jQuery(" ins .amount").prepend('our price:');
					jQuery(" del .amount").prepend('original price:');	
				</script>
					<?php
			}

			
			if($post->post_type == 'product'){
				?>
				<script>
				
					jQuery('.page-description').hide();
					jQuery('.woocommerce-result-count').hide();
					jQuery('.woocommerce-ordering').hide();
					jQuery('#container').addClass('tw-bs container');
					jQuery('#content').addClass('row');
					jQuery('#content').css('margin-left','1em');
					jQuery('#content').css('margin-right','1em');
					jQuery('.page-title').css('color','#bcbec0');
					jQuery('.page-title').css('font-size','1.618em');
					jQuery('.page-title').css('line-height','1.387em');
					jQuery('.woocommerce-message').css('border-top','3px solid #364245');
					jQuery(".onsale").css('background','none repeat scroll 0 0 #bcbec0');
					jQuery(" ins .amount").prepend('our price:');
					jQuery(" del .amount").prepend('original price:');
					jQuery('#sidebar').hide();
					
				</script>
				<?php
			}
			if($post->post_title == 'Cart'){
				?>
					<script>
						jQuery('#aspk_tokri').hide();
					</script>
				<?php
			}
		}
		function aspk_shop(){
			// category name
			/* $cat_name = $_GET['cat_name'];
			return do_shortcode( '[product_category category="'.$cat_name.'"]' );*/
			return do_shortcode( '[product_category category="appliances"]' ); 
		}
		
		function get_catogries(){
			$args = array(
				'taxonomy'  => 'category',
				'number' => 20,
				'pad_counts' => false,
			); 
			$categories = get_categories( $args );
			return $categories;
		 
		}
		function fb_login_integration() { ?>
			<script>
				  // This is called with the results from from FB.getLoginStatus().
				  function statusChangeCallback(response) {
					//console.log('statusChangeCallback');
					//console.log(response);
					// The response object is returned with a status field that lets the
					// app know the current login status of the person.
					// Full docs on the response object can be found in the documentation
					// for FB.getLoginStatus().
					if (response.status === 'connected') {
					  // Logged into your app and Facebook.
					  testAPI();
					} else if (response.status === 'not_authorized') {
					  // The person is logged into Facebook, but not your app.
					  document.getElementById('status').innerHTML = 'Please log ' +
						'into this app.';
					} else {
					  // The person is not logged into Facebook, so we're not sure if
					  // they are logged into this app or not.
					  document.getElementById('status').innerHTML = 'Please log ' +
						'into Facebook.';
					}
				  }

				  // This function is called when someone finishes with the Login
				  // Button.  See the onlogin handler attached to it in the sample
				  // code below.
				  function checkLoginState() {
					FB.getLoginStatus(function(response) {
					  statusChangeCallback(response);
					});
				  }

				  window.fbAsyncInit = function() {
				  FB.init({
					appId      : '329137903943236',
					cookie     : true,  // enable cookies to allow the server to access 
										// the session
					xfbml      : true,  // parse social plugins on this page
					version    : 'v2.1' // use version 2.1
				  });

				  FB.getLoginStatus(function(response) {
					statusChangeCallback(response);
				  });

				  };

				  // Load the SDK asynchronously
				  (function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s); js.id = id;
					js.src = "//connect.facebook.net/en_US/sdk.js";
					fjs.parentNode.insertBefore(js, fjs);
				  }(document, 'script', 'facebook-jssdk'));

				  // Here we run a very simple test of the Graph API after login is
				  // successful.  See statusChangeCallback() for when this call is made.
				  function testAPI() {
					//console.log('Welcome!  Fetching your information.... ');
					FB.api('/me', function(response) {
					  get_login_user_info(response);
					  //console.log('Successful login for: ' + response.name);
					  document.getElementById('status').innerHTML =
						'Thanks for logging in, ' + response.name + '!';
					},{scope: 'email,public_profile'});
				  }
				  function get_login_user_info(response){
					var email = response.email; 
					var first_name = response.first_name; 
					var last_name = response.last_name; 
					var id = response.id; 
						
						var data = {
								'action': 'aspk_singup_f_b',
								'email-adr': email,      
								'first_name': first_name          
							};
							var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
							jQuery.post(ajaxurl, data, function(response) {
							
							

							});
					
				  }
				 function checklogoutState(){
					FB.logout(function(response) {
					});
				}
				

				</script>

				<!--
				  Below we include the Login Button social plugin. This button uses
				  the JavaScript SDK to present a graphical Login button that triggers
				  the FB.login() function when clicked.
				-->

				<fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
				</fb:login-button>

				<div id="status">
				</div>
			<?
		}
		
		} //class ends
	}//if class ends
$agile_bt =new Agile_New_OLX();
