<?php
/*
Plugin Name: Social Sharing Plugin
Plugin URI: 
Description: Plugin to share post pages etc.
Author: Agile Solutions PK
Version: 1.0
Author URI: http://agilesolutionspk.com
*/

if ( !class_exists( 'Aspk_Social_Sharing' )){

	class Aspk_Social_Sharing{
		
		function __construct(){
			add_action( 'admin_enqueue_scripts', array(&$this,'admin_enqueue_scripts' ));
			add_action('wp_enqueue_scripts', array(&$this, 'wp_enqueue_scripts'));
			//add_shortcode( 'aspk_social_links', array(&$this,'aspk_social_links' ));
		}
		function aspk_social_links($page_share,$share_title,$share_desc,$share_image){
			alog('share_image',$share_image,__FILE__,__LINE__);
			?>
			
			<div class="social-container">
				<div class="links">
					<a href="#" data-type="twitter" data-url="<?php echo $page_share ;?>" data-description="<?php echo $share_desc ;?>" data-via="" class="prettySocial fa fa-twitter"></a>

					<a href="#" data-type="facebook" data-url="<?php echo $page_share; ?>" data-title="prettySocial - custom social share buttons." data-description="<?php echo $share_desc ;?>" data-media="<?php echo $share_image ;?>" class="prettySocial fa fa-facebook"></a>
					
					<a href="#" data-type="pinterest" data-url="<?php echo $page_share ;?>" data-summary ="<?php echo $share_desc ;?>" data-description="<?php echo $share_desc ;?>" data-media="<?php echo $share_image ;?>" class="prettySocial fa fa-pinterest"></a>

					<a href="#" data-type="linkedin" data-url="<?php echo $page_share ;?>" data-title="" data-description="<?php echo $share_desc ;?>" data-via="" data-media="<?php echo $share_image ;?>" class="prettySocial fa fa-linkedin"></a>
				</div>
			</div>

			<script type="text/javascript">
				jQuery('.prettySocial').prettySocial();
			</script>
			<?php
		}
		
		function wp_enqueue_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_style( 'font_awesome', plugins_url('font-awesome/css/font-awesome.min.css', __FILE__) );
			wp_enqueue_style( 'social_sharing_cs', plugins_url('css/social_sharing.css', __FILE__) );		
			wp_enqueue_script( 'jquery-prettySocial-minjs', plugins_url('js/jquery_prettysocial_min.js', __FILE__) );			
		}
		
		function admin_enqueue_scripts(){
			wp_enqueue_style( 'font_awesome', plugins_url('font-awesome/css/font-awesome.min.css', __FILE__) );	
			wp_enqueue_style( 'social_sharing_cs', plugins_url('css/social_sharing.css', __FILE__) );	
			wp_enqueue_script( 'jquery-prettySocial-minjs', plugins_url('js/jquery_prettysocial_min.js', __FILE__) );	
		
		}
		
	} //class ends
}//if class ends

$aspk_social_shared_links = new Aspk_Social_Sharing();
